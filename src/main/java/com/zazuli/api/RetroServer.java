package com.zazuli.api;

/**
 * Created by Mumin on 08/06/2018.
 */

public class RetroServer {
    private static final String base_url = "http://10.10.11.36/sm6/";
    private static Retrofit retrofit;
    public static Retrofit getClient()
    {
        if(retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
