package com.zazuli.api;

/**
 * Created by Mumin on 08/06/2018.
 */

public interface RestApi {
    //insert
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendkendaraan(@Field("nama") String nama,
                                      @Field("jenis") String jenis,
                                      @Field("jumlah") String jumlah,
                                      @Field("harga") String harga,
                                      @Field("diskon") String diskon;
    //read
    @GET("read.php")
    Call<ResponseModel> getBiodata();
    //update menggunakan 3 parameter
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData(@Field("id") String id,
                                   @Field("nama") String nama,
                                   @Field("jenis") String jenis,
                                   @Field("jumlah") String jumlah,
                                   @Field("harga") String harga,
                                   @Field("diskon") String diskon;
    //delete menggunakan parameter id
    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> deleteData(@Field("id") String id);

    Call<ResponseModel> updateData(String iddata, String s, String s1);

    Call<ResponseModel> sendkendaraan(String snama, String sjenis, String sjumlah, String sharga, String sdiskon);
}

