package com.zazuli.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.zazuli.model.MainActivity;
import com.zazuli.uas_crud.R;

/**
 * Created by Mumin on 08/06/2018.
 */

public class RecylerAdapter extends
        RecyclerView.Adapter<RecylerAdapter.MyHolder> {List<DataModel> mList ;
    Context ctx;
    public RecylerAdapter(Context ctx, List<DataModel> mList) {
        this.mList = mList;
        this.ctx = ctx;
    }
    @Override
    public RecylerAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist, parent, false);
        MyHolder holder = new MyHolder(layout);
        return holder;
    }
    @Override
    public void onBindViewHolder(RecylerAdapter.MyHolder holder,
                                 final int position) {
        holder.nama.setText(mList.get(position).getNama());
        holder.jenis.setText(mList.get(position).getjns());
        holder.jumlah.setText(mList.get(position).getjmlh());
        holder.harga.setText(mList.get(position).gethrg());
        holder.diskon.setText(mList.get(position).getdsk());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {@Override
        public void onClick(View view) {
            Intent goInput = new Intent(ctx,MainActivity.class);
            try {
                goInput.putExtra("id", mList.get(position).getid());
                goInput.putExtra("nama", mList.get(position).getnama());
                goInput.putExtra("jenis", mList.get(position).getjns());
                goInput.putExtra("jumlah", mList.get(position).getjmlh());
                goInput.putExtra("harga", mList.get(position).gethrg());
                goInput.putExtra("diskon", mList.get(position).getdsk());
                ctx.startActivity(goInput);
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(ctx, "Error data " +e, Toast.LENGTH_SHORT).show();
            }
        }
        });
    }
    @Override
    public int getItemCount()
    {
        return mList.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView nama, jenis, jumlah, harga, diskon;
        DataModel dataModel;
        public MyHolder(View v)
        {
            super(v);
            nama = (TextView) v.findViewById(R.id.tvnama);
            jenis = (TextView) v.findViewById(R.id.tvjns);
            jumlah = (TextView) v.findViewById(R.id.tvjmlh);
            harga = (TextView) v.findViewById(R.id.tvhrg);
            diskon = (TextView) v.findViewById(R.id.tvdsk);
        }
    }
}
